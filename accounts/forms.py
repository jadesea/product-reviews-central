from django import forms


class loginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=20,
        widget=forms.PasswordInput,
    )


class signupForm(forms.Form):
    first_name = forms.CharField(max_length=100)
    last_name = forms.CharField(max_length=100)
    email = forms.EmailField()
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=20,
        widget=forms.PasswordInput,
    )
    confirm_password = forms.CharField(
        max_length=20,
        widget=forms.PasswordInput,
    )

    def clean(self):
        cleaned_data = super().clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")
        if password != confirm_password:
            msg = "passwords do not match"
            self.add_error("confirm_password", msg)
