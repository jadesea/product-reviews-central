from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from accounts.forms import loginForm, signupForm


# Create your views here.
def login_(request):
    if request.method == "POST":
        form = loginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = loginForm()
    context = {
        "form": form,
    }
    return render(request, "login.html", context)


def logout_(request):
    logout(request)
    return redirect("home")


def signup_(request):
    if request.method == "POST":
        form = signupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            confirm_password = form.cleaned_data["confirm_password"]
            email = form.cleaned_data["email"]
            first_name = form.cleaned_data["first_name"]
            last_name = form.cleaned_data["last_name"]
            if password == confirm_password:
                user = User.objects.create_user(
                    username=username,
                    password=password,
                )
                user.first_name = first_name
                user.last_name = last_name
                user.email = email
                user.save()
                login(request, user)
                return redirect("home")
    else:
        form = signupForm()
    context = {
        "form": form,
    }
    return render(request, "signup.html", context)
