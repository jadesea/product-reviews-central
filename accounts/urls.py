from django.urls import path
from accounts.views import login_, signup_, logout_

urlpatterns = [
    path("login/", login_, name="login"),
    path("signup/", signup_, name="signup"),
    path("logout/", logout_, name="logout"),
]
