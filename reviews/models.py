from django.db import models
from django.conf import settings


# Create your models here.


class Review(models.Model):
    name = models.CharField(max_length=250)
    rating = models.DecimalField(max_digits=3, decimal_places=1)
    review = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="reviews",
        on_delete=models.PROTECT,
    )
    product_id = models.ForeignKey(
        "products.Product",
        related_name="reviews",
        on_delete=models.PROTECT,
    )


class AggReview(models.Model):
    name = models.CharField(max_length=250)
    rating = models.DecimalField(max_digits=3, decimal_places=1)
    review = models.TextField()
    url = models.URLField(max_length=200)
    date_added = models.DateTimeField(auto_now_add=True)
    product_id = models.ForeignKey(
        "products.Product",
        related_name="agg_reviews",
        on_delete=models.PROTECT,
    )
