# Generated by Django 4.2.7 on 2023-11-08 22:22

from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Product",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("brand", models.CharField(max_length=100)),
                ("name", models.CharField(max_length=200)),
                ("year", models.IntegerField()),
                ("model_number", models.CharField(max_length=100)),
                ("UPC", models.CharField(max_length=20)),
                ("ASIN", models.CharField(blank=True, max_length=10)),
                ("description", models.TextField(blank=True)),
                (
                    "aggregate_rating",
                    models.DecimalField(
                        blank=True, decimal_places=1, max_digits=3
                    ),
                ),
                ("product_id", models.CharField(max_length=200)),
            ],
        ),
    ]
