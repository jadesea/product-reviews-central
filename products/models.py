from django.db import models


# Create your models here.
class Product(models.Model):
    brand = models.CharField(max_length=100)
    name = models.CharField(max_length=200)
    year = models.IntegerField()
    model_number = models.CharField(max_length=100)
    UPC = models.CharField(max_length=20)
    ASIN = models.CharField(max_length=10, blank=True)
    description = models.TextField(blank=True)
    aggregate_rating = models.DecimalField(
        max_digits=3,
        decimal_places=1,
        blank=True,
    )
    product_id = models.CharField(max_length=200, blank=True, null=True)
