from django.urls import path
from products.views import show_brands
urlpatterns = [
    path("home/", show_brands, name="home"),
]