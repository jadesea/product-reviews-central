from django.shortcuts import render, redirect
from products.models import Product


# Create your views here.
def show_brands(request):
    products = Product.objects.all()
    context = {
        "products": products,
    }
    return render(request, "home.html", context)
